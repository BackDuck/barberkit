package ru.rubiconmd.barberkit.ui.registration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.rubiconmd.barberkit.api.ApiFactory;
import ru.rubiconmd.barberkit.model.InputError;
import ru.rubiconmd.barberkit.model.Login;
import ru.rubiconmd.barberkit.model.Registration;
import ru.rubiconmd.barberkit.utils.Validation;

/**
 * Created by Nurshat on 06.02.2018.
 */

public class RegistrationPresenter implements RegistrationContract.Presenter {
    private RegistrationContract.View view;

    public RegistrationPresenter(RegistrationContract.View view) {
        this.view = view;
    }

    @Override
    public void registration(String name, String lastname, String email, String password, String cPass, String phone) {
        Registration registration = new Registration("testm", "testm", "testm@ya.ru", "111111", "89033071092");
        ApiFactory.getRegistrationService()
                .registrate(registration)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(r -> System.out.println(r.getToken()));
    }

    @Override
    public void onFocusChanged(String name, String lastname, String email, String password, String cPass, String phone) {

        Map<String, String> map = new HashMap<>();
        map.put("name", Validation.validateFields(name));
        map.put("lName", Validation.validateFields(lastname));
        map.put("email", Validation.validateEmail(email));
        map.put("cPass", Validation.validateCPassword(password, cPass));
        map.put("pass", Validation.validatePassword(password));
        map.put("phone", Validation.validatePhone(phone));

        view.setError(map);
    }
}
