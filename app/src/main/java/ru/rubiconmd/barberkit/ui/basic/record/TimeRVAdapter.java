package ru.rubiconmd.barberkit.ui.basic.record;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.rubiconmd.barberkit.R;
import ru.rubiconmd.barberkit.model.Time;

/**
 * Created by Nurshat on 09.02.2018.
 */

public class TimeRVAdapter extends RecyclerView.Adapter<TimeRVAdapter.TimeViewHolder> {
    private List<Time> times;
    private int viewWidth;
    public static class TimeViewHolder extends RecyclerView.ViewHolder {
        TextView time;


        TimeViewHolder(View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);

        }
    }

    public TimeRVAdapter(List<Time> times) {
        this.times = times;
    }


    @Override
    public TimeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_layout, parent, false);
        viewWidth = v.getWidth();
        return new TimeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TimeViewHolder holder, int position) {
        holder.time.setText(String.format("%s : %s", times.get(position).getHour(), times.get(position).getMinute()));


    }

    @Override
    public int getItemCount() {
        return times.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public int getViewWidth() {
        return viewWidth;
    }
}