package ru.rubiconmd.barberkit.ui.registration;

import java.util.Map;

import ru.rubiconmd.barberkit.ui.base.BaseView;

/**
 * Created by Nurshat on 06.02.2018.
 */

interface RegistrationContract {

    interface View extends BaseView {

        void setError(Map<String, String> errors);

    }

    interface Presenter {

        void registration(String name, String lastname, String email, String password, String cPass, String phone);
        void onFocusChanged(String name, String lastname, String email, String password, String cPass, String phone);

    }


}