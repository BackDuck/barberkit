package ru.rubiconmd.barberkit.ui.basic;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import ru.rubiconmd.barberkit.R;
import ru.rubiconmd.barberkit.ui.basic.contacts.ContactsFragment;
import ru.rubiconmd.barberkit.ui.basic.info.InfoFragment;
import ru.rubiconmd.barberkit.ui.basic.record.RecordFragment;

public class BasicActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private ContactsFragment contactsFragment;
    private InfoFragment infoFragment;
    private RecordFragment recordFragment;
    private FragmentTransaction fTransaction;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            fTransaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fTransaction.replace(R.id.container, infoFragment);
                    fTransaction.commit();
                    return true;
                case R.id.navigation_dashboard:
                    fTransaction.replace(R.id.container, recordFragment);
                    fTransaction.commit();
                    return true;
                case R.id.navigation_notifications:
                    fTransaction.replace(R.id.container, contactsFragment);
                    fTransaction.commit();
                    return true;
            }


            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic);
        Toolbar toolbar = findViewById(R.id.toolbarBasic);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        contactsFragment = new ContactsFragment();
        infoFragment = new InfoFragment();
        recordFragment = new RecordFragment();


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
