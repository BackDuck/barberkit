package ru.rubiconmd.barberkit.ui.basic.record;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.rubiconmd.barberkit.R;
import ru.rubiconmd.barberkit.model.Master;

/**
 * Created by Nurshat on 09.02.2018.
 */

public class MastersRVAdapter extends RecyclerView.Adapter<MastersRVAdapter.MasterViewHolder> {
    private List<Master> masters;

    public static class MasterViewHolder extends RecyclerView.ViewHolder {
        ImageView masterPhoto;
        TextView masterName;
        TextView masterPrice;


        MasterViewHolder(View itemView) {
            super(itemView);
            masterName = itemView.findViewById(R.id.master_name);
            masterPrice = itemView.findViewById(R.id.master_price);

        }
    }

    public MastersRVAdapter(List<Master> masters) {
        this.masters = masters;
    }


    @Override
    public MasterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.master_cv, parent, false);
        return new MasterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MasterViewHolder holder, int position) {
        holder.masterName.setText(String.format("%s %s", masters.get(position).getName(), masters.get(position).getLastname()));
        holder.masterPrice.setText(masters.get(position).getPrice());

    }

    @Override
    public int getItemCount() {
        return masters.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}