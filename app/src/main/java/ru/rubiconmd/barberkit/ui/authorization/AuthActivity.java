package ru.rubiconmd.barberkit.ui.authorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import io.reactivex.disposables.Disposable;
import ru.rubiconmd.barberkit.App;
import ru.rubiconmd.barberkit.MainActivity;
import ru.rubiconmd.barberkit.R;
import ru.rubiconmd.barberkit.ui.registration.RegistrationActivity;

public class AuthActivity extends AppCompatActivity implements AuthContract.View {

    private AuthContract.Presenter presenter;
    private EditText email, password;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        presenter = new AuthPresenter(this);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        progressBar = findViewById(R.id.loginProgress);


    }

    public void loginBtnClick(View v) {
        presenter.login(email.getText().toString(), password.getText().toString());
    }

    public void registrationBtnClick(View v){
        Intent intent = new Intent(App.getContext(), RegistrationActivity.class);
        startActivity(intent);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showLoading(Disposable disposable) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void handleError(Throwable error) {

    }
}
