package ru.rubiconmd.barberkit.ui.authorization;

import android.content.Intent;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.rubiconmd.barberkit.App;
import ru.rubiconmd.barberkit.MainActivity;
import ru.rubiconmd.barberkit.api.ApiFactory;
import ru.rubiconmd.barberkit.model.Login;
import ru.rubiconmd.barberkit.ui.basic.BasicActivity;
import ru.rubiconmd.barberkit.utils.PreferencesManager;

/**
 * Created by Nurshat on 05.02.2018.
 */

public class AuthPresenter implements AuthContract.Presenter {
    private AuthContract.View view;

    public AuthPresenter(AuthContract.View view) {
        this.view = view;
    }

    @Override
    public void login(String email, String password) {

        Login login = new Login(email, password);

        ApiFactory.getLoginService()
                .authorize(login)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(view::showLoading)
                .doOnTerminate(view::hideLoading)
                .subscribe(r -> success(r.getToken()), e-> System.err.println(e.getMessage()));

    }

    private void success(String token){
        PreferencesManager.saveToken(token, App.getContext());
        Intent intent = new Intent(App.getContext(), BasicActivity.class);
        App.getContext().startActivity(intent);
    }
}
