package ru.rubiconmd.barberkit.ui.basic.record;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;

import ru.rubiconmd.barberkit.App;

/**
 * Created by Nurshat on 10.02.2018.
 */

public class CenterItemLayoutManager  extends LinearLayoutManager {
    private int mParentWidth;
    private int mItemWidth;


    public CenterItemLayoutManager(Context context, int orientation, boolean reverseLayout, int mParentWidth, int mItemWidth) {
        super(context, orientation, reverseLayout);
        this.mParentWidth = mParentWidth;
        this.mItemWidth = mItemWidth;
    }

    @Override
    public int getPaddingLeft() {
       return Math.round(mParentWidth / 2f - convertDpToPx(mItemWidth) / 2f);
    }

    @Override
    public int getPaddingRight() {
        return getPaddingLeft();
    }

    private int convertDpToPx(int dp){
        return Math.round(dp*(App.getContext().getResources().getDisplayMetrics().xdpi/ DisplayMetrics.DENSITY_DEFAULT));

    }
}
