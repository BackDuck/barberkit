package ru.rubiconmd.barberkit.ui.base;

import io.reactivex.disposables.Disposable;

/**
 * Created by Nurshat on 05.02.2018.
 */

public interface BaseView {
    void hideLoading();

    void showLoading(Disposable disposable);

    void handleError(Throwable error);
}
