package ru.rubiconmd.barberkit.ui.basic.record;


import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.List;

import ru.rubiconmd.barberkit.App;
import ru.rubiconmd.barberkit.R;
import ru.rubiconmd.barberkit.model.Master;
import ru.rubiconmd.barberkit.model.Time;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecordFragment extends Fragment {
    private RecyclerView mastersRv, timeRv;
    private LinearLayoutManager llm;


    public RecordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record, container, false);

        mastersRv = view.findViewById(R.id.rv_masters);
        timeRv = view.findViewById(R.id.rv_times);
        llm = new LinearLayoutManager(App.getContext(), LinearLayoutManager.HORIZONTAL, false);
        //timeLlm = new LinearLayoutManager(App.getContext(), LinearLayoutManager.HORIZONTAL, false);
        mastersRv.setLayoutManager(llm);
       // timeRv.setLayoutManager(timeLlm);

        SnapHelper mastersSnapHelper = new LinearSnapHelper();
        mastersSnapHelper.attachToRecyclerView(mastersRv);

        SnapHelper timesSnapHelper = new LinearSnapHelper();
        timesSnapHelper.attachToRecyclerView(timeRv);

        //имитация данных полученных с сервера
        List<Master> masters = new ArrayList<>();
        masters.add(new Master("Нуршат1", "Низамов1", "", "1001"));
        masters.add(new Master("Нуршат2", "Низамов2", "", "1002"));
        masters.add(new Master("Нуршат3", "Низамов3", "", "1003"));
        masters.add(new Master("Нуршат4", "Низамов4", "", "1004"));
        masters.add(new Master("Нуршат5", "Низамов5", "", "1005"));
        MastersRVAdapter adapter = new MastersRVAdapter(masters);
        mastersRv.setAdapter(adapter);

        //имитация данных полученных с сервера
        List<Time> times = new ArrayList<>();
        times.add(new Time("10", "00"));
        times.add(new Time("10", "30"));
        times.add(new Time("11", "00"));
        times.add(new Time("10", "30"));
        times.add(new Time("12", "00"));
        times.add(new Time("10", "30"));
        TimeRVAdapter timeAdapter = new TimeRVAdapter(times);
        timeRv.setAdapter(timeAdapter);


        ViewTreeObserver vto = timeRv.getViewTreeObserver();
        if (vto.isAlive()) {
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int viewWidth = timeRv.getMeasuredWidth();
                    CenterItemLayoutManager timeLlm = new CenterItemLayoutManager(App.getContext(), LinearLayoutManager.HORIZONTAL, false, viewWidth, 86);
                    timeRv.setLayoutManager(timeLlm);

                    timeRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }

        return view;

    }

}
