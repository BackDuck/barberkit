package ru.rubiconmd.barberkit.ui.authorization;

import ru.rubiconmd.barberkit.ui.base.BaseView;

/**
 * Created by Nurshat on 05.02.2018.
 */

interface AuthContract {

    interface View extends BaseView{

    }

    interface Presenter {

        void login(String email, String password);

    }


}
