package ru.rubiconmd.barberkit.ui.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.Map;

import io.reactivex.disposables.Disposable;
import ru.rubiconmd.barberkit.App;
import ru.rubiconmd.barberkit.R;
import ru.rubiconmd.barberkit.ui.authorization.AuthActivity;

public class RegistrationActivity extends AppCompatActivity implements RegistrationContract.View, View.OnFocusChangeListener {

    private RegistrationContract.Presenter presenter;
    private EditText name, lastname, phone, email, pass, cPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        name = findViewById(R.id.regName);
        lastname = findViewById(R.id.regLName);
        phone = findViewById(R.id.regPhone);
        email = findViewById(R.id.regEmail);
        pass = findViewById(R.id.regPass);
        cPass = findViewById(R.id.regCPass);

        name.setError("error");
        name.setOnFocusChangeListener(this);
        lastname.setOnFocusChangeListener(this);
        phone.setOnFocusChangeListener(this);
        email.setOnFocusChangeListener(this);
        pass.setOnFocusChangeListener(this);
        cPass.setOnFocusChangeListener(this);

        presenter = new RegistrationPresenter(this);
//        presenter.registration();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return false;

    }

    public void regClick(View v){
        presenter.registration(name.getText().toString(),
                lastname.getText().toString(),
                email.getText().toString(),
                pass.getText().toString(),
                cPass.getText().toString(),
                phone.getText().toString());
    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showLoading(Disposable disposable) {

    }

    @Override
    public void handleError(Throwable error) {

    }

    @Override
    public void onFocusChange(View view, boolean b) {
        presenter.onFocusChanged(name.getText().toString(),
                lastname.getText().toString(),
                email.getText().toString(),
                pass.getText().toString(),
                cPass.getText().toString(),
                phone.getText().toString());
    }

    @Override
    public void setError(Map<String, String> errors) {
        name.setError(errors.get("name"));
        lastname.setError(errors.get("lName"));
        email.setError(errors.get("email"));
        phone.setError(errors.get("phone"));
        cPass.setError(errors.get("cPass"));
        pass.setError(errors.get("pass"));
    }
}
