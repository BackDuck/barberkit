package ru.rubiconmd.barberkit.api;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import ru.rubiconmd.barberkit.model.Registration;
import ru.rubiconmd.barberkit.model.Token;

/**
 * Created by Nurshat on 06.02.2018.
 */

public interface RegistrationService {


    @POST("registration/client")
    Observable<Token> registrate(@Body Registration registration);


}
