package ru.rubiconmd.barberkit.api;

import android.content.Context;
import android.support.annotation.NonNull;


import java.io.IOException;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.rubiconmd.barberkit.utils.PreferencesManager;


public final class TokenInterceptor implements Interceptor {

    private Context context;

    private TokenInterceptor(Context context) {
        this.context = context;
    }

    @NonNull
    public static Interceptor create(Context context) {
        return new TokenInterceptor(context);
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        String token = PreferencesManager.getToken(context);
        Headers.Builder header = new Headers.Builder();
        if (token != null && !token.isEmpty()) {
            header.add("AUTHORIZATION", String.format("Bearer %s", token));
        }
        Request newRequest = request.newBuilder()
                .headers(header.build())
                .build();
        return chain.proceed(newRequest);
    }


}
