package ru.rubiconmd.barberkit.api;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import ru.rubiconmd.barberkit.model.Login;
import ru.rubiconmd.barberkit.model.Token;

/**
 * Created by Nurshat on 03.02.2018.
 */
public interface LoginService {


    @POST("auth/login")
    Observable<Token> authorize(@Body Login login);


}