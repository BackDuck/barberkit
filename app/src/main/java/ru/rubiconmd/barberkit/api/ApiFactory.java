package ru.rubiconmd.barberkit.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.rubiconmd.barberkit.App;
import ru.rubiconmd.barberkit.BuildConfig;



public final class ApiFactory {

    private static OkHttpClient sClient;

    private static volatile LoginService loginService;
    private static volatile RegistrationService registrationService;

    private ApiFactory() {
    }


    @NonNull
    public static LoginService getLoginService() {
        LoginService service = loginService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = loginService;
                if (service == null) {
                    service = loginService = buildRetrofit().create(LoginService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    public static RegistrationService getRegistrationService() {
        RegistrationService service = registrationService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = registrationService;
                if (service == null) {
                    service = registrationService = buildRetrofit().create(RegistrationService.class);
                }
            }
        }
        return service;
    }

    @NonNull
    private static Retrofit buildRetrofit() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(getClient())
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }


    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(TokenInterceptor.create(App.getContext()))
                .build();
    }


    private static class NullOnEmptyConverterFactory extends Converter.Factory {
        @Nullable
        @Override
        public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
            final Converter<ResponseBody, ?> delegate = retrofit.nextResponseBodyConverter(this, type, annotations);
            return (Converter<ResponseBody, Object>) body -> {
                if (body.contentLength() == 0) return null;
                return delegate.convert(body);
            };
        }
    }

}
