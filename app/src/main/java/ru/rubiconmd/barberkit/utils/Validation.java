package ru.rubiconmd.barberkit.utils;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;

import ru.rubiconmd.barberkit.App;
import ru.rubiconmd.barberkit.R;

public class Validation {

    public static String validateFields(String name) {
        if (TextUtils.isEmpty(name)) {
            return App.getContext().getString(R.string.require_field);
        }

        return null;
    }

    public static String validateEmail(String string) {
        if (TextUtils.isEmpty(string)) return App.getContext().getString(R.string.require_field);
        if(!Patterns.EMAIL_ADDRESS.matcher(string).matches()) return App.getContext().getString(R.string.email_error);

        return null;
    }

    public static String validatePhone(String string) {

        if (TextUtils.isEmpty(string)) return App.getContext().getString(R.string.require_field);
        if(!Patterns.PHONE.matcher(string).matches()) return App.getContext().getString(R.string.phone_error);

        return null;
    }

    public static String validatePassword(String value1) {
        if (TextUtils.isEmpty(value1)) return App.getContext().getString(R.string.require_field);

        if(value1.length()<6) return App.getContext().getString(R.string.pass_length_error);

        return null;

    }
    public static String validateCPassword(String value1, String value2) {
        if (TextUtils.isEmpty(value1)) return App.getContext().getString(R.string.require_field);
        if(!value1.contentEquals(value2)) return App.getContext().getString(R.string.pass_repeat_error);

        return null;

    }

    public static boolean isNullOrEmpty(@Nullable String s) {
        return s == null || s.length() == 0 || s.trim().length() == 0;
    }
}
