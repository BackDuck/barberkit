package ru.rubiconmd.barberkit.model;

/**
 * Created by Nurshat on 10.02.2018.
 */

public class Time {
    private String hour;
    private String minute;

    public Time(String hour, String minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }
}
