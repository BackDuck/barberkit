package ru.rubiconmd.barberkit.model;

/**
 * Created by Nurshat on 06.02.2018.
 */

public class InputError {
    private String id;
    private String error;

    public InputError(String id, String error) {
        this.id = id;
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
