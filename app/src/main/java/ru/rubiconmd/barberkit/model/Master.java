package ru.rubiconmd.barberkit.model;

/**
 * Created by Nurshat on 09.02.2018.
 */

public class Master {
    private String name;
    private String lastname;
    private String photoURL;
    private String price;

    public Master(String name, String lastname, String photoURL, String price) {
        this.name = name;
        this.lastname = lastname;
        this.photoURL = photoURL;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
