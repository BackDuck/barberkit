package ru.rubiconmd.barberkit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.rubiconmd.barberkit.api.ApiFactory;
import ru.rubiconmd.barberkit.model.Login;
import ru.rubiconmd.barberkit.ui.authorization.AuthActivity;
import ru.rubiconmd.barberkit.ui.basic.BasicActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(MainActivity.this, BasicActivity.class);
        startActivity(intent);
    }
}
